<?php 
    include('../login_check.php');
    include('../db_connection.php');

    if($_GET){

        $cid = filter_input(INPUT_GET,'cid',FILTER_SANITIZE_SPECIAL_CHARS);

        if($cid === ''){
            echo "All parameter are required";
            exit;
        }
        if($code_view = $conn->query("SELECT u.uid,u.u_name,u.name,a.description,c.code FROM code_snipptes c,assignments a,users u WHERE c.uid=u.uid and c.aid=a.aid and c.cid = $cid")){
            $code_view = $code_view->fetch_assoc();
?>  

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/11.6.0/styles/default.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/11.6.0/highlight.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/highlight.js@11.6.0/styles/codepen-embed.css">
    <script src="//cdn.quilljs.com/1.3.6/quill.js"></script>
    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
    <title>Code view</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins&display=swap');

* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

body {
    min-height: 100vh;
    overflow-x: hidden;
    background: url('https://images.unsplash.com/photo-1579547621700-03c2c337370a?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mjl8fGNvbG9yZnVsfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=600&q=60');
    background-size: cover;
}

#main {
    letter-spacing: 0.3px;
    font-size: 0.9rem;
    min-height: 10rem;
    width: 97vw;
    font-weight: 600;
    padding: 8px 10px;
    margin: 1rem 10px;
    border-radius: 1rem;
    color: white;
    box-shadow: 1px 1px 10px black;
    backdrop-filter: blur(15px);
    background: linear-gradient(to right bottom, rgba(255, 255, 255, 0.269), rgba(255, 255, 255, 0.169));
}

code {
    /* user-select: none;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none; */
    padding: 10px 1rem;
    border-radius: 5px;
    font-size: 1rem;
    overflow-x: auto;
}

#pre {
    min-height: 1rem;
}

h3 {
    font-family: 'Poppins', sans-serif;
    letter-spacing: 1px;
    user-select: none;
    text-indent: 0.5rem;
}

footer {
    font-family: 'Poppins', sans-serif;
    text-align: center;
    font-size: 1.1rem;
    color: white;
}

@media screen and (max-width: 700px) {
    #main {
        margin: 0 5px;
    }

    h3 {
        font-size: 1.5rem;
        margin: 0.2rem 0.4rem;
        text-indent: 0;
    }

    h5 {
        font-size: 0.9rem;
        padding-top: 0.3rem;
    }
}
    </style>    
</head>

<body>
    <div id="main">
        <h3 id="descreption"><?php echo $code_view["description"] . " | " . $code_view["u_name"]. " | ".$code_view["name"] ?></h3>
        <br>
        <pre id="pre">
            <code id="code">
            
            </code>
        </pre>
    </div>
    <footer>
    </footer>
</body>
<script>
    code = new Quill('#code') 
    code.root.innerHTML = decodeURIComponent("<?php echo base64_decode($code_view["code"]) ?>");
    hljs.highlightAll();
</script>
</html>
<head>
<?php }
        else{
            print_r(json_encode(["msg" => "FAILED"]));
        }
    }
    else{
        print_r(json_encode(["msg" => "GET REQUIRED"]));
    }
?>