<?php 
    include('../login_check.php');
    include('../db_connection.php');

    if($_GET){

        $aid = filter_input(INPUT_GET,'aid',FILTER_SANITIZE_SPECIAL_CHARS);

        if($aid === ''){
            echo "All parameter are required";
            exit;
        }
        
        ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="//cdn.quilljs.com/1.3.6/quill.js"></script>
    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
    <link rel="stylesheet" href="../public/styles.css"><link rel="icon" type="image/x-icon" href="../images/power-icon.png">
    <title>Code archives</title>
</head>
<?php
    $section_name = $conn->query("SELECT * FROM subjects WHERE sid=(SELECT sid FROM assignments WHERE aid=$aid)");
    $assign =  $conn->query("SELECT * FROM assignments WHERE aid=$aid");
    $total_users = $conn->query("SELECT count(uid) as 'total_users' FROM users");
    $total_admin = $conn->query("SELECT count(uid) as 'total_admin' FROM admin");
    
    if($code_saved_by_users = $conn->query("SELECT u.uid,u.u_name,u.name,c.code,c.cid FROM users u,code_snipptes c WHERE c.uid=u.uid and aid=$aid")){
        $code_saved_by_users = $code_saved_by_users->fetch_all();
?>
<body>
    <main class="body">
        <div id="spin" style="display: none;"></div>
        <div class="spin"></div>
        <section class="header">
            <header class="nav-bar-head">
                <nav calss="nav-bar">
                    <div class="nav-div">
                    <img src="../images/power-icon.png" alt="Image">
                    <ul>
                        <li><button class="bnt-nav" id="home">Home</button></li>
                        <!-- <li><button class="bnt-nav">Code</button></li> -->
                        <li><button class="bnt-nav" id="out">Logout</button></li>
                    </ul>
                </div>
                <div id="user"><?php echo $_SESSION["name"];?> </div>
                </nav>
            </header>
        </section>
        <div class="main">
            <section class="content">
                <div class="content-space">
                    <?php if(count($code_saved_by_users) == 0) {?>
                        <div class="section">
                            <h1>#<?php echo $section_name->fetch_assoc()['s_name'] .  ' | &nbsp; '. $assign->fetch_assoc()["description"] . '  &nbsp;&nbsp; <i class="fa-solid fa-arrow-right"></i> &nbsp;&nbsp; ' . count($code_saved_by_users) . '/' .$total_users->fetch_assoc()['total_users'] - $total_admin->fetch_assoc()['total_admin']; ?></h1>
                            <div class="card">
                                <p class="desc">No code been uploaded.</p>
                            </div>
                        </div>
                    <?php }else{ ?>
                    <div class="section">
                        <h1>#<?php echo $section_name->fetch_assoc()['s_name'] . ' |  &nbsp;'. $assign->fetch_assoc()["description"] . '  &nbsp;&nbsp; <i class="fa-solid fa-arrow-right"></i> &nbsp;&nbsp;' . count($code_saved_by_users) . '/' .$total_users->fetch_assoc()['total_users'] - $total_admin->fetch_assoc()['total_admin']; ?></h1>
                    <?php foreach($code_saved_by_users as $codes):  ?>
                        <?php    
                            if(count($codes) == 0):?>
                                    <div class="soon">
                                        <p class="desc">No Assignment added</p>
                                    </div>
                            <?php endif; ?>
                            <label style="display:none;" class="id"><?php echo $codes[4]?></label>
                            <label style="display:none;" class="uid"><?php echo $codes[0]?></label>
                            <div class="card" onclick="click_listen()">
                                <p class="desc"><?php echo $codes[1]?></p>
                                <i class="fa-solid fa-eye icon-del" style="color:white;"></i>
                            </div>
                            <?php endforeach; ?>
                        </div>  
                </div>
            </section>
            <?php  }?>
        </div>
        <footer>
            <div id="snackbar"></div>
        </footer>
    </main>
</body>
<script>
    var id = -1;
    function click_listen() {
    let i = 0
    let click = false
    var div_click = document.querySelectorAll(".card")
    var del_click = document.querySelectorAll(".fa")
    var label = document.querySelectorAll(".id")
    for (i = 0; i < div_click.length; i++) {
        let clicked = i
        div_click[i].addEventListener('click', (e) => {
            id = label[clicked].innerHTML;
            if (click)
                return
            location.href = './view_code.php?cid=' + id;
        })
    }
}
click_listen()
out.addEventListener('click', async (e) => {
    await fetch('/code_archives/logout.php', {
        method: 'GET',
    },).then((data) => {
        location.reload()
    }).catch((e) => console.log(e))
});

home.addEventListener('click', async (e) => {
    window.location.pathname = '/code_archives';
});

</script>
</html>
<?php
        }
        else{
            print_r(json_encode(["msg" => "FAILED"]));
        }
    }
    else{
        print_r(json_encode(["msg" => "GET REQUIRED"]));
    }
?>