<?php 
    include('../login_check.php');
    include('../db_connection.php');
        

    if($_POST){
        $aid = filter_input(INPUT_POST,'aid',FILTER_SANITIZE_SPECIAL_CHARS);
        $desc = filter_input(INPUT_POST,'description',FILTER_SANITIZE_SPECIAL_CHARS);

        if($aid === '' || empty($desc)){
            echo "All parameter are required";
            exit;
        }

        if($conn->query("UPDATE assignments SET description='$desc' WHERE aid=$aid")){
            print_r(json_encode(["msg" => "SUCCESS"]));
        }
        else{
            print_r(json_encode(["msg" => "FAILED"]));
        }
    }
    else{
        print_r(json_encode(["msg" => "POST REQUIRED"]));
    } 