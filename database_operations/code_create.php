<?php 
    include('../login_check.php');
    include('../db_connection.php');
        

    if($_POST){
        $sid = filter_input(INPUT_POST,'section',FILTER_SANITIZE_SPECIAL_CHARS);
        $desc = filter_input(INPUT_POST,'description',FILTER_SANITIZE_SPECIAL_CHARS);

        if(empty($desc)){
            echo "All parameter are required";
            exit;
        }
        if($conn->query("INSERT INTO assignments (sid,description) VALUES ($sid,'$desc')")){
            print_r(json_encode(["msg" => "SUCCESS"]));
        }
        else{
            print_r(json_encode(["msg" => "FAILED"]));
        }
    }
    else{
        print_r(json_encode(["msg" => "POST REQUIRED"]));
    } 