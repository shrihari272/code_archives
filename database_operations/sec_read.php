<?php 
    include('../login_check.php');
    include('../db_connection.php');
    $sections = $conn->query("SELECT * FROM subjects");
    if(!$sections)
        die();
    $sections = $sections->fetch_all();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="//cdn.quilljs.com/1.3.6/quill.js"></script>
    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
    <link rel="stylesheet" href="../public/styles.css"><link rel="icon" type="image/x-icon" href="../images/power-icon.png">
    <script src="../public/logic.js" defer></script>
    <title>Admin console</title>
</head>
<?php 
    $sections = $conn->query("SELECT * FROM subjects"); 
    $sections = $sections->fetch_all();
?>
<body>
    <main class="body">
        <div id="spin" style="display: none;"></div>
        <div class="spin"></div>
        <section class="header">
            <header class="nav-bar-head">
                <nav calss="nav-bar">
                    <div class="nav-div">
                    <img src="../images/power-icon.png" alt="Image">
                    <ul>
                        <li><button class="bnt-nav" id="home">Home</button></li>
                        <!-- <li><button class="bnt-nav">Code</button></li> -->
                        <li><button class="bnt-nav sec-bnt" onclick="btn_sec()">Subject</button></li>
                        <li><button class="bnt-nav" id="out">Logout</button></li>
                    </ul>
                </div>
                <div id="user"><?php echo $_SESSION["name"];?> </div>
                </nav>
            </header>
        </section>
        <div class="main">
            <section class="content">
                <div class="content-space">
                    <div class="sec-list">
                        <h1 style="align-self: flex-start;margin-left: 10px;">#Subject</h1>
                        <?php if(count($sections) == 0):?>
                            <div class="soon">
                                <p class="desc">No Subject Added</p>
                            </div>
                        <?php endif; foreach($sections as $section): ?>
                                <div class="sec-del" id="<?php echo $section[0] ?>">
                                    <p class="desc"><?php echo $section[1] ?></p>
                                </div>        
                        <?php endforeach; ?>
                    <div>
                </div>
                <button class="floating-bnt" onclick="sec_input()">+</button>
            </section>
        </div>
        <footer>
            <div id="snackbar"></div>
        </footer>
    </main>
</body>
</html>

                    