<?php 
    include('../login_check.php');
    include('../db_connection.php');
    if($_GET){

        $aid = filter_input(INPUT_GET,'aid',FILTER_SANITIZE_SPECIAL_CHARS);

        if($aid === ''){
            echo "All parameter are required";
            exit;
        }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="//cdn.quilljs.com/1.3.6/quill.js"></script>
    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
    <link rel="stylesheet" href="../public/styles.css"><link rel="icon" type="image/x-icon" href="./images/power-icon.png">
    <title>Admin console</title>
</head>
<?php 
    $section = $conn->query("SELECT s.sid,a.aid,s.s_name,a.description FROM assignments  a, subjects s WHERE a.sid=s.sid and a.aid=$aid"); 
    $section = $section->fetch_assoc();
?>
<body>
    <main class="body">
    <div id="spin" style="display: none;"></div>
        <div class="spin"></div>
        <section class="header">
            <header class="nav-bar-head">
                <nav calss="nav-bar">
                    <div class="nav-div">
                    <img src="../images/power-icon.png" alt="Image">
                    <ul>
                        <li><button class="bnt-nav" id="home">Home</button></li>
                        <!-- <li><button class="bnt-nav">Code</button></li> -->
                        <li><button class="bnt-nav" id="out">Logout</button></li>
                    </ul>
                </div>
                <div id="user"><?php echo $_SESSION["name"];?> </div>
                </nav>
            </header>
        </section>
        <section class="content-input">
            <div class="content-space-input">
                <select type="text" id="section" placeholder="Section">
                    <option value="<?php echo $section["aid"]; ?>"><?php echo $section["s_name"]; ?></option>
                </select>
                <input type="text" id="description" placeholder="Description" value="<?php echo $section["description"]; ?>">
                <button id="submit" onclick="update()">Edit</button>
            </div>
        </section>
        <footer>
            <div id="snackbar"></div>
        </footer>
    </main>
</body>
<script>
function snackBar(msg) {
    var x = document.getElementById("snackbar");
    x.innerHTML = msg
    x.className = "show"
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
}

async function update(){
    if (section.value === 'select' || description.value === '') {
        alert('All fields are required')
        return
    }
    spin.style.display = "block"
    await fetch(`./code_update_code.php`, {
        method: 'POST',
        body: `aid=${<?php echo $section["aid"]; ?>}&description=${description.value}`,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
    }).then((data) => {
        snackBar("Assignment Edited")
        setTimeout(function () { location.href = '../index.php' }, 1000);
    }).catch((e) => console.log(e))
    spin.style.display = "none"
}
out.addEventListener('click', async (e) => {
    await fetch('/code_archives/logout.php', {
        method: 'GET',
    },).then((data) => {
        location.reload()
    }).catch((e) => console.log(e))
});

home.addEventListener('click', async (e) => {
    window.location.pathname = '/code_archives';
});
</script>
</html>
<?php } ?>                