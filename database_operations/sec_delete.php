<?php 
    include('../login_check.php');
    include('../db_connection.php');

    if($_GET){

        $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_SPECIAL_CHARS);
        $section = filter_input(INPUT_GET,'section',FILTER_SANITIZE_SPECIAL_CHARS);
 
        if($id === ''|| $section === ''){
            echo "All parameter are required";
            exit;
        }
        $aids = $conn->query("SELECT aid FROM assignments WHERE sid=$id");
        foreach($aids->fetch_all() as $aid)
            $conn->query("DELETE FROM code_snipptes WHERE aid=".$aid[0]);
        $conn->query("DELETE FROM assignments WHERE sid=$id");
        
        if($conn->query("DELETE FROM subjects WHERE sid=$id")){
            print_r(json_encode(["msg" => "SUCCESS"]));
        }
        else{
            print_r(json_encode(["msg" => "FAILED"]));
        }
    }
    else{
        print_r(json_encode(["msg" => "GET REQUIRED"]));
    }
     