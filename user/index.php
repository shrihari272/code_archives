<?php
include('./user_login_check.php');
include('../db_connection.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="//cdn.quilljs.com/1.3.6/quill.js"></script>
    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
    <link rel="stylesheet" href="../public/styles.css">
    <link rel="icon" type="image/x-icon" href="./images/power-icon.png">
    <title>Code archives</title>
</head>
<?php
$sections = $conn->query("SELECT * FROM subjects");
$sections = $sections->fetch_all();
?>

<body class="user-body">
    <main class="body">
        <div id="spin" style="display: none;"></div>
        <div class="spin"></div>
        <section class="header">
            <header class="nav-bar-head">
                <nav calss="nav-bar">
                    <div class="nav-div">
                        <img src="../images/power-icon.png" alt="Image">
                        <ul>
                            <li><button class="bnt-nav" id="home">Home</button></li>
                            <!-- <li><button class="bnt-nav">Code</button></li> -->
                            <li><button class="bnt-nav" id="out">Logout</button></li>
                        </ul>
                    </div>
                    <div id="user"><?php echo $_SESSION["name"]; ?> </div>
                </nav>
            </header>
        </section>
        <div class="main">
            <section class="content">
                <div class="content-space">
                    <?php if (count($sections) == 0) : ?>
                        <div class="section">
                            <h1>#Subject 1</h1>
                            <div class="card">
                                <p class="desc">Description</p>
                            </div>
                        </div>
                        <div class="section">
                            <h1>#Subject 2</h1>
                            <div class="card">
                                <p class="desc">Description</p>
                            </div>
                        </div>
                        <div class="section">
                            <h1>#Subject 3</h1>
                            <div class="card">
                                <p class="desc">Description</p>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php foreach ($sections as $section) :  ?>
                        <div class="section">
                            <!-- <?php print_r($sections) ?> -->
                            <h1>#<?php echo $section[1]; ?></h1>
                            <?php
                            $secdata = $conn->query("SELECT * FROM assignments WHERE sid='$section[0]'");
                            if ($secdata->num_rows == 0) : ?>
                                <div class="soon">
                                    <p class="desc">No Assignment added</p>
                                </div>
                            <?php endif;
                            $secdata = $secdata->fetch_all();
                            foreach ($secdata as $data) :
                                $is_code_saved = $conn->query("SELECT * FROM code_snipptes WHERE aid='$data[0]' and uid=" . $_SESSION["uid"]);
                            ?>
                                <label style="display:none;" class="id"><?php echo $data[0] ?></label>
                                <label style="display:none;" class="sid"><?php echo $data[1] ?></label>
                                <div class="card" onclick="click_listen()">
                                    <p class="desc"><?php echo $data[2];
                                    if ($is_code_saved->num_rows > 0)
                                        echo ' - Submitted'; 
                                    ?></p>
                                    <?php
                                    if ($is_code_saved->num_rows > 0)
                                        echo '<i class="fa fa-trash-o icon-del"></i>';
                                    ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </section>
        </div>
        <footer>
            <div id="snackbar"></div>
        </footer>
    </main>
</body>
<script>
    var id = -1;

    function getItem() {
        location.href = (`./code_window.php?aid=${id}`);
    }

    function deleteAssign() {
        console.log("./delete_uploaded_assignent.php?aid=" + id);
        fetch('./delete_uploaded_assignent.php?aid=' + id, {
            method: 'GET',
        }, ).then((data) => {
            return data.text();
        }).then((data) => {
            snackBar('Saved code been deleted ...')
            console.log(data);
            location.reload();
        }).catch((e) => console.log(e))
    }

    function click_listen() {
        let i = 0
        let click = false
        var div_click = document.querySelectorAll(".card")
        var del_click = document.querySelectorAll(".icon-del")
        var edit_click = document.querySelectorAll(".icon-edit")
        var label = document.querySelectorAll(".id")

        console.log(del_click);
        console.log(label);
        
        for (i = 0; i < del_click.length; i++) {
            let clicked = i
            del_click[i].addEventListener('click', (e) => {
                click = true;
            })
        }

        for (i = 0; i < div_click.length; i++) {
            let clicked = i
            div_click[i].addEventListener('click', (e) => {
                id = label[clicked].innerHTML;
                console.log('log:',id);
                if (click)
                    return deleteAssign()
                getItem();
        })


        }
    }
    click_listen();
    out.addEventListener('click', async (e) => {
        await fetch('/code_archives/logout.php', {
            method: 'GET',
        }, ).then((data) => {
            location.reload()
        }).catch((e) => console.log(e))
    });

    home.addEventListener('click', async (e) => {
        window.location.pathname = '/code_archives';
    });

    function snackBar(msg) {
        var x = document.getElementById("snackbar");
        x.innerHTML = msg
        x.className = "show"
        setTimeout(function() {
            x.className = x.className.replace("show", "");
        }, 3000);
    }
</script>

</html>