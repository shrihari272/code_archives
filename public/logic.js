var bnt = document.querySelector('.floating-bnt');
var all_section = '';
var all_section_arrange = '';
var btn_section = '';
var id = '';
var code;
var toolbarOpt = [
    ['bold','italic','underline','strike'],
    ['code-block']
]
var options = {
modules: {
    toolbar: toolbarOpt,
},
placeholder: 'Code',
theme: 'snow'
};

function getAllItems() {
    click_listen();
}

//Change in add and back
async function change() {
    let option = ''
    await fetch('./database_operations/code_read.php').then((res) => {
        return res.json()
    })
        .then((data) => {
            for(const opt of data){
                option += `<option value="${opt[0]}">${opt[1]}</option>`
            }
        })
    if (bnt.innerText === '+') {
        var main = document.querySelector('.main')
        main.innerHTML = `<section class="content-input">
                <div class="content-space-input">
                    <select type="text" id="section" placeholder="Section">
                        <option value="select">--select--</option>
                        ${option}
                    </select>    
                    <input type="text" id="description" placeholder="Description">   
                    <button id="submit" onclick="create()">Save</button>
                </div>
                <button class="floating-bnt" onclick="change()"><</button>
            </section>`
        // code = new Quill('#code', options)
        bnt = document.querySelector('.floating-bnt')
        document.querySelector('.sec-bnt').style.display = 'none';
    }
    else if (bnt.innerText === '<') {
        location.reload()
    }
}

////Creating files
async function create() {
    if (section.value === 'select' || description.value === '') {
        alert('All field are required')
        return
    }
    spin.style.display = "block"
    await fetch('./database_operations/code_create.php', {
        method: 'POST',
        body: `section=${section.value}&description=${description.value}`,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
    }).then((result) => {
        return result.text()
        // setTimeout(function () { location.reload() }, 1000);
    }).then((result) => {
        console.log(result);
        snackBar("Code Inserted")
        setTimeout(function () { location.reload() }, 1000);
    }).catch((err) => {
        console.log(err);
    });
    spin.style.display = "none"
}


function getItem() {
    location.href = (`./database_operations/code_get.php?aid=${id}`);
}

function editItem(){
    location.href = (`./database_operations/code_update.php?aid=${id}`);
}

function click_listen() {
    let i = 0
    let click = false
    var div_click = document.querySelectorAll(".card")
    var del_click = document.querySelectorAll(".fa")
    var edit_click = document.querySelectorAll(".icon-edit")
    var label = document.querySelectorAll(".id")
    for (i = 0; i < del_click.length; i++) {
        let clicked = i
        del_click[i].addEventListener('click', async (e) => {
            id = label[clicked].innerHTML;
            click = true
            await remove();
        })
    }
    for (i = 0; i < div_click.length; i++) {
        let clicked = i
        edit_click[i].addEventListener('click', (e) => {
            id = label[clicked].innerHTML;
            if (click)
                return
            editItem();
            click = true
        })
    }
    for (i = 0; i < div_click.length; i++) {
        let clicked = i
        div_click[i].addEventListener('click', (e) => {
            id = label[clicked].innerHTML;
            if (click)
                return
            getItem();
        })
    }
}

async function update() {
    if (section.value === 'select' || description.value === '' || code.root.innerHTML === '<p><br></p>') {
        alert('All fields are required')
        return
    }
    spin.style.display = "block"
    await fetch(`./database_operations/code_update.php`, {
        method: 'POST',
        body: `id=${id}&description=${description.value}&code=${btoa(encodeURIComponent(code.root.innerHTML))}`,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
    }).then(() => {
        snackBar("Code Edited")
        setTimeout(function () { location.reload() }, 1000);
    }).catch((e) => console.log(e))
    spin.style.display = "none"
}

async function remove() {
    console.log(id);
    spin.style.display = "block"
    await fetch(`./database_operations/code_delete.php?id=${id}`)
    .then(() => {
        snackBar("Code Deleted")
        setTimeout(function () { location.reload() }, 400);
    }).catch((e) => console.log(e))
    spin.style.display = "none"
}

out.addEventListener('click', async (e) => {
    await fetch('/code_archives/logout.php', {
        method: 'GET',
    },).then((data) => {
        location.reload()
    }).catch((e) => console.log(e))
});

home.addEventListener('click', async (e) => {
    location.href = '/code_archives';
});

function snackBar(msg) {
    var x = document.getElementById("snackbar");
    x.innerHTML = msg
    x.className = "show"
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
}

 function btn_sec() {
    window.location.pathname = './code_archives/database_operations/sec_read.php'
}

async function sec_delete(id,name) {
    spin.style.display = "block"
    console.log(name);
    await fetch(`./sec_delete.php?id=${id}&section=${name}`).then((res)=> res.text()).then((res) => {
        console.log(res);
        snackBar('Loading ...');
        setTimeout(function () { snackBar('Section Deleted'); }, 1000);
        setTimeout(function () { location.reload(); }, 1000);
    })
    spin.style.display = "none"
}

async function sec_save() {
    spin.style.display = "block"
    regex = /[`~!@#$%^&*()_|+\=?;:'",.<>\{\}\[\]\\\/]/gi
    if (section.value === '') {
        alert("Section field required.")
        spin.style.display = "none"
        return
    }
    if (regex.exec(section.value)) {
        spin.style.display = "none"
        alert("Invalid name. Special characters are not allowed.");
        return
    }
    await fetch(`./sec_create.php`, {
        method: 'POST',
        body: `section=${section.value}`,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
    },).then((res) => res.json()).then(async (data) => {
        snackBar('Loading ...')
        setTimeout(function () { snackBar('Section Saved') }, 1000);
        bnt.innerHTML = '+'
        await btn_sec()
    })
    spin.style.display = "none"
}

function sec_listen() {
    let i = 0
    var div_click = document.querySelectorAll(".sec-del")
    var sec_click = document.querySelectorAll(".desc")
    for (i = 0; i < div_click.length; i++) {
        let clicked = i
        div_click[i].addEventListener('click', (e) => {
            sec_delete(div_click[clicked].id, sec_click[clicked].innerHTML);
        })
    }
}

function sec_input() {
    document.querySelector('.sec-list').innerHTML = `
    <div class="sec-input">
    <input type="text sec-text" id="section" style="width:60%;height:2.7rem;" placeholder="#Subject"> 
    <button id="submit" onclick="sec_save()" style="margin-bottom:5px; margin-left:5px;">Save</button>
    </div>
    `
    if (bnt.innerText === '<') {
        btn_sec()
        bnt.innerHTML = '+'
        return
    }
    bnt.innerHTML = "<"
}

sec_listen();
getAllItems();