<?php

    session_start();

    if(!isset($_SESSION['admin']))
        header('Location: /code_archives/login.php');
    else{
        if($_SESSION['admin'] != 1)
            header('Location: /code_archives/user/index.php');
    }
    
