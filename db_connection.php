<?php

    define('DBHOST','localhost');
    define('DBUSER','root');
    define('DBPASS','root');
    define('DBNAME','code_archives');

    $conn = new mysqli(DBHOST,DBUSER,DBPASS,DBNAME);

    if($conn->connect_errno){
        echo $conn->connect_errno;
        die();
    }